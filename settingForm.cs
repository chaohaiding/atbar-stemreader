﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Text;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Forms;
namespace ATbarSTEM
{
    public partial class SettingForm : Form
    {
        public ColorDialog colorDlg1 = new ColorDialog();
        public FontDialog fontDlg1 = new FontDialog();
        private SpeechSynthesizer synthesizer = new SpeechSynthesizer();
        public SettingForm()
        {
            InitializeComponent();
            label6.BackColor = Properties.Settings.Default.textAreaFontColour;
            richTextBox1.ForeColor = Properties.Settings.Default.textAreaFontColour;
            richTextBox1.BackColor = Properties.Settings.Default.textAreaBackgroundColour;
            richTextBox1.Font = Properties.Settings.Default.textAreaFont;
            label4.BackColor = Properties.Settings.Default.textAreaBackgroundColour;
            label3.BackColor = Properties.Settings.Default.highLightColour;

            comboBox1.SelectedIndex = Properties.Settings.Default.highLightMethod;
            checkBox1.Checked = Properties.Settings.Default.textAreaDispaly;
            comboBoxSelectVoice.Items.AddRange(Properties.Settings.Default.speakVoicesCollection.Split(',').ToArray());
            comboBoxSelectVoice.SelectedItem = Properties.Settings.Default.speakSelectedVoice;
            checkBoxScreenOverlay.Checked = Properties.Settings.Default.screenOverlayLaunch;
            trackBarSpeakRate.Value = Properties.Settings.Default.speakSpeedRate;
            trackBarColourOverlay.Value = Properties.Settings.Default.screenOverlayTransparency;
            higlight_MethdoChanged();
        }

        private void settingForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = label3.BackColor;

            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label3.BackColor = colorDlg1.Color;
                higlight_MethdoChanged();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = label4.BackColor;
            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                 label4.BackColor = colorDlg1.Color;
                 richTextBox1.BackColor = colorDlg1.Color;
            }
        }

        private void confirmBtn_Click(object sender, EventArgs e)
        {
            if (label3.BackColor == label4.BackColor)
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Highlight colour and background colour could not be same", "Setting Error", MessageBoxButton.OK);
            }
            else if (label3.BackColor == label6.BackColor)
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Highlight colour and font colour could not be same", "Setting Error", MessageBoxButton.OK);
            }
            else if (label4.BackColor == label6.BackColor)
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("Background colour and font colour could not be same", "Setting Error", MessageBoxButton.OK);
            }
            else
            {
                Properties.Settings.Default.highLightColour = label3.BackColor;
                Properties.Settings.Default.highLightMethod = comboBox1.SelectedIndex;

                Properties.Settings.Default.textAreaBackgroundColour = label4.BackColor;
                Properties.Settings.Default.textAreaFontColour = label6.BackColor;
                Properties.Settings.Default.textAreaFont = richTextBox1.Font;
                Properties.Settings.Default.textAreaDispaly = checkBox1.Checked;
                Properties.Settings.Default.Save();
                this.Close();
            }
           
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = label3.BackColor;

            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label3.BackColor = colorDlg1.Color;
                higlight_MethdoChanged();
 
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = label4.BackColor;
            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label4.BackColor = colorDlg1.Color;
                richTextBox1.BackColor = colorDlg1.Color;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            fontDlg1.ShowColor = true;
            fontDlg1.Font = richTextBox1.Font;
            fontDlg1.Color = label6.BackColor;
            //fontDlg1.AllowSimulations = false;
            //fontDlg1.AllowScriptChange=false;
            fontDlg1.FontMustExist = true;
            if (fontDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                richTextBox1.Font = fontDlg1.Font;
                label6.BackColor = fontDlg1.Color;
                richTextBox1.ForeColor = fontDlg1.Color;
            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = label6.BackColor;
            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                label6.BackColor = colorDlg1.Color;
                richTextBox1.ForeColor = colorDlg1.Color;
            }
        }

        private void higlight_MethdoChanged()
        {
            if (comboBox1.SelectedIndex == 1)
            {
                richTextBox1.Select(richTextBox1.Text.IndexOf("This"), "This".Length);
                richTextBox1.SelectionStart = 0;
                richTextBox1.SelectionLength = richTextBox1.SelectedText.Length;
                richTextBox1.SelectionBackColor = label3.BackColor;
            }
            else if (comboBox1.SelectedIndex == 2)
            {
                richTextBox1.SelectionStart = 0;
                richTextBox1.SelectionLength = richTextBox1.Text.Length;
                richTextBox1.SelectionBackColor = label3.BackColor;

            }
            else
            {
                richTextBox1.SelectionStart = 0;
                richTextBox1.SelectionLength = richTextBox1.Text.Length;
                richTextBox1.SelectionBackColor = Color.Transparent;
            }
        }
        
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            higlight_MethdoChanged();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            colorDlg1.AllowFullOpen = true;
            colorDlg1.Color = Properties.Settings.Default.screenOverlayColour;
            if (colorDlg1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                Properties.Settings.Default.screenOverlayColour = colorDlg1.Color;
            }   
        }

        private void buttonVoiceSave_Click(object sender, EventArgs e)
        {
            speechExampleCancell();
            Properties.Settings.Default.speakSpeedRate = trackBarSpeakRate.Value;
            Properties.Settings.Default.speakSelectedVoice = comboBoxSelectVoice.Text;
            Properties.Settings.Default.Save();
            this.Close();
        }

        private void buttonVoiceCancel_Click(object sender, EventArgs e)
        {
            speechExampleCancell();
            //this.Close();
        }

        private void buttonPlayExample_Click(object sender, EventArgs e)
        {
            speechExampleCancell();
            synthesizer.SelectVoice(comboBoxSelectVoice.Text);
            synthesizer.Rate = trackBarSpeakRate.Value;
            synthesizer.SpeakAsync(labelVoiceExample.Text);
        }

        private void buttonColourCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonColourSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.screenOverlayLaunch = checkBoxScreenOverlay.Checked;
            Properties.Settings.Default.screenOverlayTransparency = trackBarColourOverlay.Value;

            Properties.Settings.Default.Save();
            this.Close();
        }

        private void speechExampleCancell()
        {
            if (synthesizer.State == SynthesizerState.Speaking)
            {
                synthesizer.SpeakAsyncCancelAll();
            }
        }

        private void trackBarSpeakRate_Scroll(object sender, EventArgs e)
        {
            speechExampleCancell();
        }

        private void comboBoxSelectVoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            speechExampleCancell();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            System.Diagnostics.Process.Start("http://www.atbar.org/atbar-windows");    

        }

  
        private void linkLabel1_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://access.ecs.soton.ac.uk/"); 
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.atbar.org/atbar-windows/"); 
        }

        





    }
}
