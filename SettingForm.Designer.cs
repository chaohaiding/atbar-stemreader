﻿using System;
using System.Windows.Forms;
namespace ATbarSTEM
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingForm));
            this.colorDialogBackground = new System.Windows.Forms.ColorDialog();
            this.colorDialogHighlight = new System.Windows.Forms.ColorDialog();
            this.Colour = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxScreenOverlay = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.buttonSelectOverlayColour = new System.Windows.Forms.Button();
            this.trackBarColourOverlay = new System.Windows.Forms.TrackBar();
            this.label17 = new System.Windows.Forms.Label();
            this.buttonColourCancel = new System.Windows.Forms.Button();
            this.buttonColourSave = new System.Windows.Forms.Button();
            this.Voice = new System.Windows.Forms.TabPage();
            this.groupBoxVoice = new System.Windows.Forms.GroupBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.buttonPlayExample = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelVoiceExample = new System.Windows.Forms.Label();
            this.trackBarSpeakRate = new System.Windows.Forms.TrackBar();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxSelectVoice = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.buttonVoiceCancel = new System.Windows.Forms.Button();
            this.buttonVoiceSave = new System.Windows.Forms.Button();
            this.Display = new System.Windows.Forms.TabPage();
            this.groupBoxReading = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.confirmBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.About = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.button5 = new System.Windows.Forms.Button();
            this.Colour.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColourOverlay)).BeginInit();
            this.Voice.SuspendLayout();
            this.groupBoxVoice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeakRate)).BeginInit();
            this.Display.SuspendLayout();
            this.groupBoxReading.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.About.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Colour
            // 
            this.Colour.BackColor = System.Drawing.SystemColors.Control;
            this.Colour.Controls.Add(this.groupBox3);
            this.Colour.Location = new System.Drawing.Point(4, 22);
            this.Colour.Name = "Colour";
            this.Colour.Padding = new System.Windows.Forms.Padding(3);
            this.Colour.Size = new System.Drawing.Size(440, 332);
            this.Colour.TabIndex = 2;
            this.Colour.Text = "Colour";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox3.Controls.Add(this.checkBoxScreenOverlay);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.buttonSelectOverlayColour);
            this.groupBox3.Controls.Add(this.trackBarColourOverlay);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.buttonColourCancel);
            this.groupBox3.Controls.Add(this.buttonColourSave);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(437, 326);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Colour overlay settings";
            // 
            // checkBoxScreenOverlay
            // 
            this.checkBoxScreenOverlay.AutoSize = true;
            this.checkBoxScreenOverlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxScreenOverlay.Location = new System.Drawing.Point(22, 29);
            this.checkBoxScreenOverlay.Name = "checkBoxScreenOverlay";
            this.checkBoxScreenOverlay.Size = new System.Drawing.Size(190, 19);
            this.checkBoxScreenOverlay.TabIndex = 0;
            this.checkBoxScreenOverlay.Text = "Apply colour overlay on launch";
            this.checkBoxScreenOverlay.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(233, 116);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(39, 15);
            this.label18.TabIndex = 26;
            this.label18.Text = "100%";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(170, 116);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 15);
            this.label15.TabIndex = 25;
            this.label15.Text = "50%";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(116, 116);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(25, 15);
            this.label16.TabIndex = 24;
            this.label16.Text = "0%";
            // 
            // buttonSelectOverlayColour
            // 
            this.buttonSelectOverlayColour.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonSelectOverlayColour.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonSelectOverlayColour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSelectOverlayColour.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonSelectOverlayColour.Location = new System.Drawing.Point(22, 68);
            this.buttonSelectOverlayColour.Name = "buttonSelectOverlayColour";
            this.buttonSelectOverlayColour.Size = new System.Drawing.Size(238, 25);
            this.buttonSelectOverlayColour.TabIndex = 1;
            this.buttonSelectOverlayColour.Text = "Select Overlay Colour";
            this.buttonSelectOverlayColour.UseVisualStyleBackColor = false;
            this.buttonSelectOverlayColour.Click += new System.EventHandler(this.button7_Click);
            // 
            // trackBarColourOverlay
            // 
            this.trackBarColourOverlay.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarColourOverlay.Location = new System.Drawing.Point(108, 143);
            this.trackBarColourOverlay.Name = "trackBarColourOverlay";
            this.trackBarColourOverlay.Size = new System.Drawing.Size(157, 45);
            this.trackBarColourOverlay.TabIndex = 2;
            this.trackBarColourOverlay.TickStyle = System.Windows.Forms.TickStyle.Both;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(22, 154);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(81, 15);
            this.label17.TabIndex = 20;
            this.label17.Text = "Transparency";
            // 
            // buttonColourCancel
            // 
            this.buttonColourCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonColourCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonColourCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonColourCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonColourCancel.Location = new System.Drawing.Point(360, 288);
            this.buttonColourCancel.Name = "buttonColourCancel";
            this.buttonColourCancel.Size = new System.Drawing.Size(70, 30);
            this.buttonColourCancel.TabIndex = 4;
            this.buttonColourCancel.Text = "Cancel";
            this.buttonColourCancel.UseVisualStyleBackColor = false;
            this.buttonColourCancel.Click += new System.EventHandler(this.buttonColourCancel_Click);
            // 
            // buttonColourSave
            // 
            this.buttonColourSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonColourSave.DialogResult = System.Windows.Forms.DialogResult.Retry;
            this.buttonColourSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonColourSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonColourSave.Location = new System.Drawing.Point(270, 288);
            this.buttonColourSave.Name = "buttonColourSave";
            this.buttonColourSave.Size = new System.Drawing.Size(70, 30);
            this.buttonColourSave.TabIndex = 3;
            this.buttonColourSave.Text = "Save";
            this.buttonColourSave.UseVisualStyleBackColor = false;
            this.buttonColourSave.Click += new System.EventHandler(this.buttonColourSave_Click);
            // 
            // Voice
            // 
            this.Voice.Controls.Add(this.groupBoxVoice);
            this.Voice.Location = new System.Drawing.Point(4, 22);
            this.Voice.Name = "Voice";
            this.Voice.Padding = new System.Windows.Forms.Padding(3);
            this.Voice.Size = new System.Drawing.Size(440, 332);
            this.Voice.TabIndex = 1;
            this.Voice.Text = "Voice";
            this.Voice.UseVisualStyleBackColor = true;
            // 
            // groupBoxVoice
            // 
            this.groupBoxVoice.BackColor = System.Drawing.SystemColors.Control;
            this.groupBoxVoice.Controls.Add(this.label14);
            this.groupBoxVoice.Controls.Add(this.label13);
            this.groupBoxVoice.Controls.Add(this.label12);
            this.groupBoxVoice.Controls.Add(this.buttonPlayExample);
            this.groupBoxVoice.Controls.Add(this.groupBox2);
            this.groupBoxVoice.Controls.Add(this.trackBarSpeakRate);
            this.groupBoxVoice.Controls.Add(this.label10);
            this.groupBoxVoice.Controls.Add(this.comboBoxSelectVoice);
            this.groupBoxVoice.Controls.Add(this.label9);
            this.groupBoxVoice.Controls.Add(this.buttonVoiceCancel);
            this.groupBoxVoice.Controls.Add(this.buttonVoiceSave);
            this.groupBoxVoice.Location = new System.Drawing.Point(3, 3);
            this.groupBoxVoice.Name = "groupBoxVoice";
            this.groupBoxVoice.Size = new System.Drawing.Size(437, 326);
            this.groupBoxVoice.TabIndex = 0;
            this.groupBoxVoice.TabStop = false;
            this.groupBoxVoice.Text = "Text to speech settings";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(237, 66);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 15);
            this.label14.TabIndex = 26;
            this.label14.Text = "Fast";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(168, 66);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 15);
            this.label13.TabIndex = 25;
            this.label13.Text = "Normal";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(109, 66);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 15);
            this.label12.TabIndex = 24;
            this.label12.Text = "Slow";
            // 
            // buttonPlayExample
            // 
            this.buttonPlayExample.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonPlayExample.BackgroundImage = global::ATbarSTEM.Properties.Resources.Speak;
            this.buttonPlayExample.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonPlayExample.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPlayExample.Image = global::ATbarSTEM.Properties.Resources.Speak;
            this.buttonPlayExample.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPlayExample.Location = new System.Drawing.Point(25, 156);
            this.buttonPlayExample.Name = "buttonPlayExample";
            this.buttonPlayExample.Size = new System.Drawing.Size(238, 25);
            this.buttonPlayExample.TabIndex = 4;
            this.buttonPlayExample.Text = "Play speech example";
            this.buttonPlayExample.UseVisualStyleBackColor = false;
            this.buttonPlayExample.Click += new System.EventHandler(this.buttonPlayExample_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.labelVoiceExample);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(22, 194);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(251, 82);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "example";
            // 
            // labelVoiceExample
            // 
            this.labelVoiceExample.BackColor = System.Drawing.SystemColors.Control;
            this.labelVoiceExample.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVoiceExample.ForeColor = System.Drawing.Color.Black;
            this.labelVoiceExample.Location = new System.Drawing.Point(12, 21);
            this.labelVoiceExample.Name = "labelVoiceExample";
            this.labelVoiceExample.Size = new System.Drawing.Size(225, 58);
            this.labelVoiceExample.TabIndex = 5;
            this.labelVoiceExample.Text = "Hello, this is what my voice settings sound like";
            // 
            // trackBarSpeakRate
            // 
            this.trackBarSpeakRate.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.trackBarSpeakRate.LargeChange = 4;
            this.trackBarSpeakRate.Location = new System.Drawing.Point(108, 93);
            this.trackBarSpeakRate.Minimum = -10;
            this.trackBarSpeakRate.Name = "trackBarSpeakRate";
            this.trackBarSpeakRate.Size = new System.Drawing.Size(157, 45);
            this.trackBarSpeakRate.SmallChange = 2;
            this.trackBarSpeakRate.TabIndex = 3;
            this.trackBarSpeakRate.TickFrequency = 2;
            this.trackBarSpeakRate.TickStyle = System.Windows.Forms.TickStyle.Both;
            this.trackBarSpeakRate.Scroll += new System.EventHandler(this.trackBarSpeakRate_Scroll);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 15);
            this.label10.TabIndex = 20;
            this.label10.Text = "Voice speed";
            // 
            // comboBoxSelectVoice
            // 
            this.comboBoxSelectVoice.AllowDrop = true;
            this.comboBoxSelectVoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSelectVoice.FormattingEnabled = true;
            this.comboBoxSelectVoice.Location = new System.Drawing.Point(123, 28);
            this.comboBoxSelectVoice.Name = "comboBoxSelectVoice";
            this.comboBoxSelectVoice.Size = new System.Drawing.Size(142, 21);
            this.comboBoxSelectVoice.TabIndex = 2;
            this.comboBoxSelectVoice.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelectVoice_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(19, 28);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Select voice";
            // 
            // buttonVoiceCancel
            // 
            this.buttonVoiceCancel.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonVoiceCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonVoiceCancel.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonVoiceCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVoiceCancel.Location = new System.Drawing.Point(360, 288);
            this.buttonVoiceCancel.Name = "buttonVoiceCancel";
            this.buttonVoiceCancel.Size = new System.Drawing.Size(70, 30);
            this.buttonVoiceCancel.TabIndex = 7;
            this.buttonVoiceCancel.Text = "Cancel";
            this.buttonVoiceCancel.UseVisualStyleBackColor = false;
            this.buttonVoiceCancel.Click += new System.EventHandler(this.buttonVoiceCancel_Click);
            // 
            // buttonVoiceSave
            // 
            this.buttonVoiceSave.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.buttonVoiceSave.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.buttonVoiceSave.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.buttonVoiceSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVoiceSave.Location = new System.Drawing.Point(270, 288);
            this.buttonVoiceSave.Name = "buttonVoiceSave";
            this.buttonVoiceSave.Size = new System.Drawing.Size(70, 30);
            this.buttonVoiceSave.TabIndex = 6;
            this.buttonVoiceSave.Text = "Save";
            this.buttonVoiceSave.UseVisualStyleBackColor = false;
            this.buttonVoiceSave.Click += new System.EventHandler(this.buttonVoiceSave_Click);
            // 
            // Display
            // 
            this.Display.Controls.Add(this.groupBoxReading);
            this.Display.Location = new System.Drawing.Point(4, 22);
            this.Display.Name = "Display";
            this.Display.Padding = new System.Windows.Forms.Padding(3);
            this.Display.Size = new System.Drawing.Size(440, 332);
            this.Display.TabIndex = 0;
            this.Display.Text = "Display";
            this.Display.UseVisualStyleBackColor = true;
            // 
            // groupBoxReading
            // 
            this.groupBoxReading.BackColor = System.Drawing.SystemColors.Control;
            this.groupBoxReading.Controls.Add(this.groupBox1);
            this.groupBoxReading.Controls.Add(this.comboBox1);
            this.groupBoxReading.Controls.Add(this.label8);
            this.groupBoxReading.Controls.Add(this.cancelBtn);
            this.groupBoxReading.Controls.Add(this.confirmBtn);
            this.groupBoxReading.Controls.Add(this.label6);
            this.groupBoxReading.Controls.Add(this.button3);
            this.groupBoxReading.Controls.Add(this.label5);
            this.groupBoxReading.Controls.Add(this.label2);
            this.groupBoxReading.Controls.Add(this.label3);
            this.groupBoxReading.Controls.Add(this.button2);
            this.groupBoxReading.Controls.Add(this.label4);
            this.groupBoxReading.Controls.Add(this.checkBox1);
            this.groupBoxReading.Controls.Add(this.label1);
            this.groupBoxReading.Controls.Add(this.button1);
            this.groupBoxReading.Location = new System.Drawing.Point(3, 3);
            this.groupBoxReading.Name = "groupBoxReading";
            this.groupBoxReading.Size = new System.Drawing.Size(437, 326);
            this.groupBoxReading.TabIndex = 8;
            this.groupBoxReading.TabStop = false;
            this.groupBoxReading.Text = "Reading Window";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(258, 19);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(174, 241);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "example";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.White;
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.richTextBox1.ForeColor = System.Drawing.Color.Black;
            this.richTextBox1.Location = new System.Drawing.Point(3, 17);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(168, 221);
            this.richTextBox1.TabIndex = 11;
            this.richTextBox1.Text = "This is the preview example for the text settings.";
            // 
            // comboBox1
            // 
            this.comboBox1.AllowDrop = true;
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "Each word",
            "Each sentence"});
            this.comboBox1.Location = new System.Drawing.Point(141, 177);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(100, 21);
            this.comboBox1.TabIndex = 7;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(11, 177);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Highlight Text";
            // 
            // cancelBtn
            // 
            this.cancelBtn.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cancelBtn.Location = new System.Drawing.Point(360, 288);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(70, 30);
            this.cancelBtn.TabIndex = 13;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = false;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // confirmBtn
            // 
            this.confirmBtn.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.confirmBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.confirmBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.confirmBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.confirmBtn.Location = new System.Drawing.Point(270, 288);
            this.confirmBtn.Name = "confirmBtn";
            this.confirmBtn.Size = new System.Drawing.Size(70, 30);
            this.confirmBtn.TabIndex = 12;
            this.confirmBtn.Text = "Save";
            this.confirmBtn.UseVisualStyleBackColor = false;
            this.confirmBtn.Click += new System.EventHandler(this.confirmBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Black;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(14, 89);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(94, 18);
            this.label6.TabIndex = 14;
            this.label6.Text = "                            ";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // button3
            // 
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(141, 84);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(100, 25);
            this.button3.TabIndex = 3;
            this.button3.Text = "select font...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(107, 15);
            this.label5.TabIndex = 2;
            this.label5.Text = "Select Font Colour";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 15);
            this.label2.TabIndex = 4;
            this.label2.Text = "Select Background Colour";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Yellow;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(14, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "                            ";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button2
            // 
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(141, 138);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 25);
            this.button2.TabIndex = 5;
            this.button2.Text = "select colour ...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.White;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(14, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 18);
            this.label4.TabIndex = 7;
            this.label4.Text = "                            ";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(14, 30);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(156, 19);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Text = "Display reading window";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 211);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label1.Size = new System.Drawing.Size(132, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "Select Highlight Colour";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(141, 237);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 25);
            this.button1.TabIndex = 9;
            this.button1.Text = "select colour ...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Display);
            this.tabControl1.Controls.Add(this.Voice);
            this.tabControl1.Controls.Add(this.Colour);
            this.tabControl1.Controls.Add(this.About);
            this.tabControl1.Location = new System.Drawing.Point(1, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(448, 358);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // About
            // 
            this.About.Controls.Add(this.groupBox4);
            this.About.Location = new System.Drawing.Point(4, 22);
            this.About.Name = "About";
            this.About.Padding = new System.Windows.Forms.Padding(3);
            this.About.Size = new System.Drawing.Size(440, 332);
            this.About.TabIndex = 3;
            this.About.Text = "About";
            this.About.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox4.Controls.Add(this.linkLabel2);
            this.groupBox4.Controls.Add(this.linkLabel1);
            this.groupBox4.Controls.Add(this.richTextBox2);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Location = new System.Drawing.Point(2, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(437, 326);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "About";
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.linkLabel2.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel2.Location = new System.Drawing.Point(14, 55);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(201, 15);
            this.linkLabel2.TabIndex = 12;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "http://www.atbar.org/atbar-windows/";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.HotTrack;
            this.linkLabel1.Location = new System.Drawing.Point(255, 78);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(134, 15);
            this.linkLabel1.TabIndex = 11;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "ECS Accessibility Team";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked_1);
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.SystemColors.Control;
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.richTextBox2.Location = new System.Drawing.Point(16, 34);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(374, 148);
            this.richTextBox2.TabIndex = 10;
            this.richTextBox2.Text = resources.GetString("richTextBox2.Text");
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(360, 288);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(70, 30);
            this.button5.TabIndex = 4;
            this.button5.Text = "Cancel";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // SettingForm
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.Window;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(449, 362);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SettingForm";
            this.RightToLeftLayout = true;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Setting - ATBar";
            this.Colour.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarColourOverlay)).EndInit();
            this.Voice.ResumeLayout(false);
            this.groupBoxVoice.ResumeLayout(false);
            this.groupBoxVoice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarSpeakRate)).EndInit();
            this.Display.ResumeLayout(false);
            this.groupBoxReading.ResumeLayout(false);
            this.groupBoxReading.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.About.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        private void tabControl1_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.KeyCode == Keys.Enter)
            {
                switch(this.tabControl1.SelectedIndex){
                    case 0:
                        this.confirmBtn.PerformClick();
                        break;
                    case 1:
                        this.buttonVoiceSave.PerformClick();
                        break;
                    case 2:
                        this.buttonColourSave.PerformClick();
                        break;
                    default:
                        break;
                }
            }
        }




        #endregion

        private System.Windows.Forms.ColorDialog colorDialogBackground;
        private System.Windows.Forms.ColorDialog colorDialogHighlight;
        private System.Windows.Forms.TabPage Colour;
        private System.Windows.Forms.TabPage Voice;
        private System.Windows.Forms.TabPage Display;
        private System.Windows.Forms.GroupBox groupBoxReading;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.Button confirmBtn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBoxVoice;
        private System.Windows.Forms.ComboBox comboBoxSelectVoice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button buttonVoiceCancel;
        private System.Windows.Forms.Button buttonVoiceSave;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label labelVoiceExample;
        private System.Windows.Forms.TrackBar trackBarSpeakRate;
        private System.Windows.Forms.Button buttonPlayExample;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button buttonSelectOverlayColour;
        private System.Windows.Forms.TrackBar trackBarColourOverlay;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button buttonColourCancel;
        private System.Windows.Forms.Button buttonColourSave;
        private System.Windows.Forms.CheckBox checkBoxScreenOverlay;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private TabPage About;
        private GroupBox groupBox4;
        private Button button5;
        private RichTextBox richTextBox2;
        private LinkLabel linkLabel1;
        private LinkLabel linkLabel2;
    }
}