﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ATbarSTEM
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {

        protected override void OnStartup(StartupEventArgs e)
        {
           base.OnStartup(e);

           StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);

            /*
           using (Mutex mutex = new Mutex(false, "Global\\" + appGuid))
           {
               if (!mutex.WaitOne(0, false))
               {
                   MessageBox.Show("Instance already running");
                   return;
               }
               else
               {
                   StartupUri = new Uri("MainWindow.xaml", UriKind.Relative);
               }
               
           }*/

        }
    }
}
