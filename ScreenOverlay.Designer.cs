﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ATbarSTEM
{
    partial class ScreenOverlay
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        /// 
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

  
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(Form tocover)
        {
            // ScreenOverlay
            this.FormBorderStyle = FormBorderStyle.None;
            this.ControlBox = false;

            Rectangle r = new Rectangle();
            foreach (Screen s in Screen.AllScreens)
            {
                r = Rectangle.Union(r, s.Bounds);
            }

            this.Top = r.Top;
            this.Left = r.Left;
            this.Width = r.Width;
            this.Height = r.Height;
            /*
            int screenTop = SystemInformation.VirtualScreen.Top;
            int screenWidth = SystemInformation.VirtualScreen.Width;
            int screenLeft = SystemInformation.VirtualScreen.Left;
            this.Location = new System.Drawing.Point(screenLeft, screenTop);*/

            this.Location = new System.Drawing.Point(this.Left, this.Top);

            //this.WindowState = FormWindowState.Maximized;
            this.ShowInTaskbar = false;
            //this.StartPosition = FormStartPosition.Manual;
            this.AutoScaleMode = AutoScaleMode.None;
            this.TransparencyKey = Properties.Settings.Default.screenOverlayColour;
            this.TopMost = true;
            //int screenLeft = SystemInformation.VirtualScreen.Left;
            //int screenTop = SystemInformation.VirtualScreen.Top;
            //int screenWidth = SystemInformation.VirtualScreen.Width;
            //int screenHeight = SystemInformation.VirtualScreen.Height;
            //this.Size = new System.Drawing.Size(screenWidth, screenHeight);
            //this.Location = new System.Drawing.Point(screenLeft, screenTop);
            //this.Location = Screen.AllScreens[1].WorkingArea.Location;
        }

        #endregion
        public enum GWL
        {
            ExStyle = -20
        }

        public enum WS_EX
        {
            Transparent = 0x20,
            Layered = 0x80000
        }

        public enum LWA
        {
            ColorKey = 0x1,
            Alpha = 0x2
        }
        [DllImport("user32.dll", EntryPoint = "GetWindowLong")]
        public static extern int GetWindowLong(IntPtr hWnd, GWL nIndex);

        [DllImport("user32.dll", EntryPoint = "SetWindowLong")]
        public static extern int SetWindowLong(IntPtr hWnd, GWL nIndex, int dwNewLong);

        [DllImport("user32.dll", EntryPoint = "SetLayeredWindowAttributes")]
        public static extern bool SetLayeredWindowAttributes(IntPtr hWnd, int crKey, byte alpha, LWA dwFlags);

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            int wl = GetWindowLong(this.Handle, GWL.ExStyle);
            wl = wl | 0x80000 | 0x20;
            SetWindowLong(this.Handle, GWL.ExStyle, wl);
            byte bAlpha = (byte)(255-(int)(Properties.Settings.Default.screenOverlayTransparency*25.5)); //0-255
            SetLayeredWindowAttributes(this.Handle, 0, bAlpha, LWA.Alpha);
        }
    }
}