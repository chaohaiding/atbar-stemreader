﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Speech.Synthesis;
using System.Globalization;
using System.Threading;

using MColor = System.Windows.Media.Color;
using DColor = System.Drawing.Color;
using System.Windows.Forms;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Speech.AudioFormat;
using System.Windows.Interop;
using Microsoft.Win32;
using System.Diagnostics;
using System.Xml;
using System.Xml.Schema;
using System.IO;


namespace ATbarSTEM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    
    public partial class MainWindow : Window
    {   
        //speech synthesizer
        private SpeechSynthesizer synthesizer;
        public SolidColorBrush highlightColor;
        public SolidColorBrush backgroudColor;
     
        private int currentSpaceHeight=0;
        private int currentSpaceWidth=0;
        private int totalWidth=0;
        private int totalHeight=0;

        private int TEXT_BOX_WIDTH=440;
        private int TEXT_BOX_HEIGHT=278;
        private bool APP_OPEN_STATUS = false;
        private int currentLine;
        private int currentHeight;

        private List<int> sentence_highlight_index=new List<int>();
        private int current_senetence_index=0;

        private string currentText = "";
        System.Windows.Forms.ColorDialog colorDlg = new System.Windows.Forms.ColorDialog();
        
        TextPointer textPointer;
        TextRange currentTR;

        ScreenOverlay screen_overlay = new ScreenOverlay();

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

        [DllImport("User32.dll", CharSet = CharSet.Auto)]
        public static extern bool ChangeClipboardChain(IntPtr hWndRemove, IntPtr hWndNewNext);

        private const int WM_DRAWCLIPBOARD = 49643;//0x0308;        // WM_DRAWCLIPBOARD message
        private IntPtr _clipboardViewerNext;                // Our variable that will hold the value to identify the next window in the clipboard viewer chain

        public MainWindow()
        {
            InitializeComponent();
            Window_Loaded(this, null);
            Window_Initial();
            //_clipboardViewerNext = SetClipboardViewer(new WindowInteropHelper(this).Handle);      // Adds our form to the chain of clipboard viewers.
            Closing += this.OnWindowClosing;
        }


        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
            //HwndSource source = (HwndSource) PresentationSource.FromVisual(this) as HwndSource;
            HwndSource source = (HwndSource) HwndSource.FromVisual(this);
            _clipboardViewerNext = SetClipboardViewer(source.Handle);
            source.AddHook(WndProc);
        }


        private IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            // Handle messages...
            if (wParam.ToString() == "82" && msg == WM_DRAWCLIPBOARD)
            {
                if (APP_OPEN_STATUS)
                {
                    System.Windows.IDataObject iData = System.Windows.Clipboard.GetDataObject();      // Clipboard's data
                    if (iData.GetDataPresent(System.Windows.DataFormats.Text))
                    {
                        string text = (string)iData.GetData(System.Windows.DataFormats.Text);      // Clipboard text
                        newTextSelected(text);
                    }
                }
                else
                {
                    APP_OPEN_STATUS = true;
                }
                
            }

            return IntPtr.Zero;
        }

        private void OnWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.screenOverlayStatus = false;
            Properties.Settings.Default.Save();
            ChangeClipboardChain(new WindowInteropHelper(this).Handle, _clipboardViewerNext); 
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {   
            synthesizer = new SpeechSynthesizer();
            #region synthesizer eventes
            synthesizer.StateChanged += new EventHandler<StateChangedEventArgs>(synthesizer_StateChanged);
            synthesizer.SpeakStarted += new EventHandler<SpeakStartedEventArgs>(synthesizer_SpeakStarted);
            synthesizer.SpeakProgress += new EventHandler<SpeakProgressEventArgs>(synthesizer_SpeakProgress);
            synthesizer.SpeakCompleted += new EventHandler<SpeakCompletedEventArgs>(synthesizer_SpeakCompleted);
            #endregion
        }

        private void Window_Initial()
        {
            if (Properties.Settings.Default.textAreaDispaly)
            {
                textBoard.Visibility = Visibility.Visible;
                this.Height = 400;
            }
            else
            {
                textBoard.Visibility = Visibility.Collapsed;
                this.Height = 120;
            }

            screen_overlay.VisibleChanged += screen_overlay_VisibleChanged;

            reading_display_setting();
            speak_setting_initial();
            colour_overlay_initial();

        }


        private void newTextSelected(string _text)
        {
            string text = Regex.Replace(_text, @"\s+", " ");

            if (text != currentText)
            {
                synthesizer.SpeakAsyncCancelAll();
                synthesizer.Resume();

                textBoard.Selection.Text = text;
                currentText = text;
                textBoard.ScrollToHome();
            }
            
        }


        private void reading_display_setting()
        {
            //Initial Highlight Colour
            var d_hightlight_colour = Properties.Settings.Default.highLightColour;
            var m_hightlight_colour = MColor.FromArgb(d_hightlight_colour.A, d_hightlight_colour.R, d_hightlight_colour.G, d_hightlight_colour.B);
            highlightColor = new SolidColorBrush(m_hightlight_colour);
            
            //Initial Background Colour
            var d_background_colour = Properties.Settings.Default.textAreaBackgroundColour;
            var m_background_colour = MColor.FromArgb(d_background_colour.A, d_background_colour.R, d_background_colour.G, d_background_colour.B);
            backgroudColor = new SolidColorBrush(m_background_colour);
            textBoard.Background = backgroudColor;

            //Initial Font Colour
            var d_foreground_colour = Properties.Settings.Default.textAreaFontColour;
            var m_foreground_colour = MColor.FromArgb(d_foreground_colour.A, d_foreground_colour.R, d_foreground_colour.G, d_foreground_colour.B);
            textBoard.Foreground = new SolidColorBrush(m_foreground_colour);
           
            //Initial Font style
            System.Drawing.Font font = Properties.Settings.Default.textAreaFont;

            //textBoard.FontFamily = new System.Windows.Media.FontFamily(font.Name);
            textBoard.FontWeight = font.Bold ? System.Windows.FontWeights.Bold : System.Windows.FontWeights.Regular;
            textBoard.FontStyle = font.Italic ? System.Windows.FontStyles.Italic : System.Windows.FontStyles.Normal;
            textBoard.Document.FontSize = font.Size * (96.0 / 72.0);
            textBoard.Document.FontFamily = new System.Windows.Media.FontFamily(font.Name);
            System.Drawing.Size textSize = TextRenderer.MeasureText(" ", font);
            currentSpaceHeight=textSize.Height;
            currentSpaceWidth=textSize.Width;
        }

        private void speak_setting_initial()
        {
            //Initial
            List<string> installedVoices = new List<string>();
            foreach (InstalledVoice voice in synthesizer.GetInstalledVoices())
            {
                VoiceInfo info = voice.VoiceInfo;
                installedVoices.Add(info.Name);
            }
            if (installedVoices.Count > 0)
            {
                Properties.Settings.Default.speakVoicesCollection = String.Join(",", installedVoices.ToArray());
                Properties.Settings.Default.speakSelectedVoice = installedVoices[0];
                Properties.Settings.Default.Save();
                speak_setting();
            }
            else
            {
                MessageBoxResult result = System.Windows.MessageBox.Show("There is no text to speech voice installed!", "Alert", MessageBoxButton.OK);
            }
        }


        private void colour_overlay_initial()
        {
            if (Properties.Settings.Default.screenOverlayLaunch)
            {
                colour_overlay_setting();
            }
        }

        private void colour_overlay_setting()
        {
            if (Properties.Settings.Default.screenOverlayColour != System.Drawing.Color.Transparent && Properties.Settings.Default.screenOverlayLaunch == true)
            {
                screen_overlay.Dispose();
                screen_overlay = new ScreenOverlay();
                screen_overlay.VisibleChanged += screen_overlay_VisibleChanged;
                screen_overlay.TopMost = true;
                screen_overlay.BackColor = Properties.Settings.Default.screenOverlayColour;             
                screen_overlay.Show();
            }
        }

        private void speak_setting()
        {
            synthesizer.SelectVoice(Properties.Settings.Default.speakSelectedVoice);
            synthesizer.Volume = Convert.ToInt32(Properties.Settings.Default.speakVolume);
            synthesizer.Rate = Convert.ToInt32(Properties.Settings.Default.speakSpeedRate);
        } 

        private void window_resize()
        {
            if (Properties.Settings.Default.textAreaDispaly)
            {
                textBoard.Visibility = Visibility.Visible;
                this.Height = 400;
            }
            else
            {
                textBoard.Visibility = Visibility.Collapsed;
                this.Height = 120;
            }
        }


        private void synthesizer_StateChanged(object sender, StateChangedEventArgs e)
        {
            if (e.State == SynthesizerState.Paused)
            {
                playBtn.Visibility = Visibility.Visible;
                pauseBtn.Visibility = Visibility.Hidden;
            }
            else if(e.State==SynthesizerState.Ready)
            {
                initial_parameters();
            }
        }


        private void synthesizer_SpeakStarted(object sender, SpeakStartedEventArgs e)
        {
            playBtn.Visibility = Visibility.Hidden;
            pauseBtn.Visibility = Visibility.Visible;
        }


        private void synthesizer_SpeakProgress(object sender, SpeakProgressEventArgs e)
        {
            //show the synthesizer's current progress
            if (Properties.Settings.Default.textAreaDispaly)
            {
                if(Properties.Settings.Default.highLightMethod==1)//each word
                {
                    HighlightWordInRichTextBox(textBoard, e.Text, highlightColor);

                }
                else if(Properties.Settings.Default.highLightMethod==2)//each sentence
                {
                    HighlightSentenceInRichTextBox(textBoard, e.Text, highlightColor);
                }
                scrollInRichTextBox(textBoard, e.Text);
            }
           
        }
        private void synthesizer_SpeakCompleted(object sender, SpeakCompletedEventArgs e)
        {
            //reset when complete
            playBtn.Visibility = Visibility.Visible;
            pauseBtn.Visibility = Visibility.Hidden;
            TextRange textRange = new TextRange(textBoard.Document.ContentStart, textBoard.Document.ContentEnd);
            textRange.ApplyPropertyValue(TextElement.BackgroundProperty, null);
            initial_parameters();
        }

        private void initial_parameters()
        {
            totalWidth = 0;
            totalHeight = 0;
            currentLine = 0;
            currentHeight = 0;
            current_senetence_index = 0;
        }

        private void button_Click_Play(object sender, RoutedEventArgs e)
        {
            //textBoard.Visibility = Visibility.Visible;
            switch (synthesizer.State)
            {
                //if synthesizer is ready
                case SynthesizerState.Ready:
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        string text = Regex.Replace(System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.UnicodeText), @"\s+", " ");
                        textBoard.Selection.Text = text;
                        currentText = text;
                        textBoard.ScrollToHome();
                        TextRange range = new TextRange(textBoard.Document.ContentStart, textBoard.Document.ContentEnd);

                        //for fix the bug :
                        //AriSLA - Italian Foundation for Research on Amyotrophic Lateral Sclerosis (ALS) - (“AriSLA” or the “Foundation”) has been investing in Italy since 2009 in the best research to find a cure for ALS. 
                        
                        string _text=Regex.Replace(range.Text, @"-", " ");

                        synthesizer.SpeakAsync(_text);

                        textPointer = textBoard.Document.ContentStart;
                        if (Properties.Settings.Default.highLightMethod == 2)//each sentence
                        {
                            sentence_highlight_index = HighlightSentencePre(range);
                        }
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(this, "There is no text in clipboard", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    
                    break;
                //if synthesizer is paused
                case SynthesizerState.Paused:
                    if (System.Windows.Clipboard.ContainsText())
                    {
                        playBtn.Visibility = Visibility.Hidden;
                        pauseBtn.Visibility = Visibility.Visible;
                        synthesizer.Resume();
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(this, "There is no text in clipboard", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    
                    break;
                //if synthesizer is speaking
                case SynthesizerState.Speaking:
                    playBtn.Visibility = Visibility.Visible;
                    pauseBtn.Visibility = Visibility.Hidden;
                    synthesizer.Pause();
                    break;
            }
        }

        private void button_Click_Clear(object sender, RoutedEventArgs e)
        {
            playBtn.Visibility = Visibility.Visible;
            pauseBtn.Visibility = Visibility.Hidden;
            System.Windows.Clipboard.Clear();
            synthesizer.SpeakAsyncCancelAll();
            synthesizer.Resume();
            textBoard.Selection.Text = "";
        }

       
        private void button_Click_Math(object sender, RoutedEventArgs e)
        {
            string text = String.Empty;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            if (System.Windows.Clipboard.ContainsText())
            {   
                text = System.Windows.Clipboard.GetText(System.Windows.TextDataFormat.UnicodeText);
                //Extract mathml from xml
                int firstMathMLIndex = text.IndexOf("<math");
                int firstMMLIndex = text.IndexOf("<mml:math");
                //Get mathml or mml 
                if (firstMathMLIndex != -1 || firstMMLIndex != -1)
                {
                    string mathml = "";
                    string mathmlEndString = "</math>";
                    string mmlEndString = "</mml:math>";
                    int firstMathMLIndexEnd = text.IndexOf(mathmlEndString);
                    int firstMMLIndexEnd = text.IndexOf(mmlEndString);

                    //Only mathml
                    if (firstMathMLIndex != -1 && firstMMLIndex == -1)
                    {
                        mathml = text.Substring(firstMathMLIndex, firstMathMLIndexEnd - firstMathMLIndex + mathmlEndString.Length); 
                    }


                    //Only mml
                    else if (firstMMLIndex != -1 && firstMathMLIndex == -1)
                    {
                        mathml = text.Substring(firstMMLIndex, firstMMLIndexEnd - firstMMLIndex+ mmlEndString.Length);
                    }
                    //Both mathml and mml
                    else
                    {
                        if (firstMathMLIndex <= firstMMLIndex)
                        {
                            mathml = text.Substring(firstMathMLIndex, firstMathMLIndexEnd-firstMathMLIndex + mathmlEndString.Length);
                        }
                        else
                        {
                            mathml = text.Substring(firstMMLIndex, firstMMLIndexEnd-firstMMLIndexEnd + mmlEndString.Length); 
                        }
                    }

                    if (!validate_MathML(mathml))
                    {
                        System.Windows.MessageBox.Show(this, " The MathML object is not valid!", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                        send_STEMReader(mathml);
                    }
                }
                else
                {

                    RegistryKey regKey = Registry.CurrentUser.OpenSubKey(@"Software\ATbar\MsgBoxCheck", true);
                    if (regKey == null)
                    {
                        regKey = Registry.CurrentUser.CreateSubKey(@"Software\ATbar\MsgBoxCheck");
                        regKey.SetValue("DontShowAgain", "False");
                    }
                    
                    string dotShowAgain = regKey.GetValue("DontShowAgain").ToString();
                    
                    if (dotShowAgain == "True")
                        {
                            string processToSTEMReader = regKey.GetValue("ProcessToSTEMReader").ToString();
                            if (processToSTEMReader == "True")
                            {
                                send_STEMReader(text);
                            }

                        }
                    else
                        {
                            MsgBoxCheck.MessageBox dlg = new MsgBoxCheck.MessageBox();
                            System.Windows.Forms.DialogResult dr = dlg.Show(@"Software\ATbar\MsgBoxCheck", "DontShowAgain", System.Windows.Forms.DialogResult.OK,
                                    "Don't ask me this again",
                                    "MathML was not detected on the clipboard but it may contain LaTeX. Do you wish to proceed with sending clipboard content to STEMReader?",
                                    "Alert",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (dr == System.Windows.Forms.DialogResult.Yes)
                            {
                                regKey.SetValue("ProcessToSTEMReader", "True");
                                regKey.Close();
                                send_STEMReader(text);
                            }
                            else if (dr == System.Windows.Forms.DialogResult.No)
                            {
                                regKey.SetValue("ProcessToSTEMReader", "False");
                                regKey.Close();
                            }
                        }
                }
            }
            else
            {
               System.Windows.MessageBox.Show(this, "There is no text in clipboard", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }

        }

        /**
        * This function is to send the mathml or mml to the STEMReader
        * */
        private void send_STEMReader(string mathml)
        {
            mathml=change_MathColour(mathml);
            string exefilename = "nw.exe";

            var installPathWin64 = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("Wow6432node").OpenSubKey("StemReader Windows");
            //TODO: need to check with win32 StemReader installpath
            var installPathWin32 = Registry.LocalMachine.OpenSubKey("Software").OpenSubKey("StemReader Windows");

            var installPath = installPathWin64;

            if (installPathWin64 == null && installPathWin32==null)
            {
                System.Windows.MessageBox.Show(this, "Can't find StemReader Windows. Please install StemReader Windows first.", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                if (installPathWin64 == null && installPathWin32 != null)
                {
                    installPath = installPathWin32;
                }

                //check_STEMReader("nw");
                var processFile = installPath.GetValue("installPath") + "\\" + exefilename;
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = processFile;
                //find the selected ruleset
                startInfo.Arguments = System.Uri.EscapeDataString(mathml);
                try
                {
                    Process.Start(startInfo);
                }
                catch (Exception)
                {
                    System.Windows.MessageBox.Show(this, "Can't launch StemReader Windows. If you have installed the StemReader, please uninstall the SteamReader and reinstall StemReader Windows.", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
               
            }
        }


        private String change_MathColour(string mathml)
        {
            XmlDocument xmlDoc = new XmlDocument();
            try
            {
                xmlDoc.LoadXml(mathml);
                XmlElement rootEle = (XmlElement)xmlDoc.DocumentElement;
                if (rootEle != null)
                {
                    rootEle.SetAttribute("mathcolor", "black"); // Set to new value.
                }
                return xmlDoc.OuterXml;

            }
            catch (XmlException)
            {
                return mathml;
            }
        }


        /**
         * In case for mutliple instance.
         * */
        private void check_STEMReader(string name)
        {
            foreach (Process clsProcess in Process.GetProcesses())
            {
                Console.WriteLine(clsProcess.ProcessName);


                if (clsProcess.ProcessName.Equals(name))
                {
                    try
                    {
                        if (!clsProcess.HasExited)
                        {
                            clsProcess.Kill();
                        }
                    }
                    finally
                    {
                        clsProcess.Dispose();
                    }
                }
               
            }
        }

        /**
        * 
        * Private function to examine the validation of the input xml is mathml.
        * */
        private bool validate_MathML(string xml_document)
        {
            // Create the XmlSchemaSet class.
            XmlSchemaSet sc = new XmlSchemaSet();

            // Add the schema to the collection.
            //sc.Add("xmlns:mml", "http://www.w3.org/1998/Math/MathML");

            sc.Add("http://www.w3.org/1998/Math/MathML", "http://www.w3.org/Math/XMLSchema/mathml3/mathml3.xsd");

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ValidationType = ValidationType.Schema;
            settings.Schemas = sc;

            using (StringReader stri = new StringReader(xml_document))
            {
                using (XmlReader reader = XmlReader.Create(stri, settings))
                {
                    try
                    {
                        /*reader.Read();*/
                        XmlDocument document = new XmlDocument();
                        document.Load(reader);
                        Console.WriteLine(xml_document);
                        return true;
                    }
                    catch(System.Xml.XmlException){
                        return false;
                    }
                    
                }
            }
        }

        private void button_Click_Colour(object sender, RoutedEventArgs e)
        {
            //Properties.Settings.Default.screenOverlayStatus == true

            if (screen_overlay.Visible==true)
            {
                screen_overlay.Hide();
            }
            else
            {
                screen_overlay.Dispose();
                screen_overlay = new ScreenOverlay();
                screen_overlay.VisibleChanged += screen_overlay_VisibleChanged;
                screen_overlay.TopMost = true;
                screen_overlay.BackColor = Properties.Settings.Default.screenOverlayColour;
                screen_overlay.Show();
                this.Activate();
                this.Focus();
                /*
                if (Properties.Settings.Default.screenOverlayColour != System.Drawing.Color.Transparent)
                {
                    screen_overlay.BackColor = Properties.Settings.Default.screenOverlayColour;
                    screen_overlay.Show();
                    this.Activate();
                    this.Focus();
                }*/
            }
        }

        void screen_overlay_VisibleChanged(object sender, EventArgs e)
        {
            if (screen_overlay.Visible == true)
            {
                Properties.Settings.Default.screenOverlayStatus = true;
            }
            else
            {
                Properties.Settings.Default.screenOverlayStatus = false;
            }

            Properties.Settings.Default.Save();
            
        }
        
        private void button_Click_Setting(object sender, RoutedEventArgs e)
        {
            SettingForm setting_form = new SettingForm();
            screen_overlay.TopMost = true;
            setting_form.TopMost = true;
            setting_form.Focus();
            if (synthesizer.State==SynthesizerState.Speaking)
            {
                synthesizer.Pause();
            }

            var result=setting_form.ShowDialog();

            if (result == System.Windows.Forms.DialogResult.OK)//display
            {
                reading_display_setting();
                window_resize();
                synthesizer.SpeakAsyncCancelAll();
                synthesizer.Resume();
            }
            else if (result == System.Windows.Forms.DialogResult.Yes)//voice
            {
                speak_setting();
                synthesizer.SpeakAsyncCancelAll();
                synthesizer.Resume();
            }
            else if (result == System.Windows.Forms.DialogResult.Retry)//Colour
            {
                colour_overlay_setting();
            }

            this.Topmost = true;
            this.Focus();
        }


        private void HighlightWordInRichTextBox(System.Windows.Controls.RichTextBox richTextBox, String word, SolidColorBrush color)
        {
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);

            textRange.ApplyPropertyValue(TextElement.BackgroundProperty, null);

            //Current word at the pointer
            TextRange tr = FindWordFromPosition(textPointer, word);
            if (!object.Equals(tr, null))
            {
               currentTR = tr;
               //set the pointer to the end of "word"
               textPointer = tr.End;
               //apply highlight color
               tr.ApplyPropertyValue(TextElement.BackgroundProperty, color);
            }
        }

        private void scrollInRichTextBox(System.Windows.Controls.RichTextBox richTextBox, String word)
        {
            System.Drawing.Size textSize = TextRenderer.MeasureText(word, Properties.Settings.Default.textAreaFont);
            totalWidth = totalWidth + textSize.Width + currentSpaceWidth;
            totalHeight = totalWidth / TEXT_BOX_WIDTH*currentSpaceHeight;
            if (totalHeight > TEXT_BOX_HEIGHT / 2 && totalHeight > currentHeight)
            { 
                currentLine++;
                currentHeight = totalHeight;
                richTextBox.ScrollToVerticalOffset((double)(currentLine*currentSpaceHeight));
            }
        }

        private TextRange FindWordFromPosition(TextPointer position, string word)
        {
            while (position != null)
            {   
  
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
    
                    string textRun = position.GetTextInRun(LogicalDirection.Forward);
                    // Find the starting index of any substring that matches "word".
                    int indexInRun = textRun.IndexOf(word);
                    if (indexInRun >= 0)
                    {
                        TextPointer start = position.GetPositionAtOffset(indexInRun);
                        TextPointer end = start.GetPositionAtOffset(word.Length);
                        return new TextRange(start, end);
                    }
                }
                position = position.GetNextContextPosition(LogicalDirection.Forward);
            }
            // position will be null if "word" is not found.
            return null;
        }

        private void HighlightSentenceInRichTextBox(System.Windows.Controls.RichTextBox richTextBox, String word, SolidColorBrush color)
        {
            TextRange textRange = new TextRange(richTextBox.Document.ContentStart, richTextBox.Document.ContentEnd);    
            TextRange tr = FindSentenceFromPosition(textPointer, word);
            int[] index_sentence = sentence_highlight_index.ToArray();
            if (!object.Equals(tr, null))
            {
                textPointer = tr.End;
                currentTR = tr;
                int currentOffset = 0 - textPointer.GetOffsetToPosition(textRange.Start);
                int temp_senetence_index = 0;
                for (int i = 0; i < index_sentence.Length;i++ )
                {
                    if (currentOffset > index_sentence[i])
                    {
                        temp_senetence_index = i;
                    }
                }

                if(temp_senetence_index==0)
                {
                    current_senetence_index = temp_senetence_index;

                    if (index_sentence.Length == 1)
                    {
                        TextRange tr1 = new TextRange(textRange.Start, textRange.End);
                        tr1.ApplyPropertyValue(TextElement.BackgroundProperty, color);
                    }
                    else
                    {
                        TextRange tr1 = new TextRange(textRange.Start.GetPositionAtOffset(index_sentence[current_senetence_index]), textRange.Start.GetPositionAtOffset(index_sentence[current_senetence_index + 1] - 1));
                        tr1.ApplyPropertyValue(TextElement.BackgroundProperty, color);
                    }
                   
                }
                else if (temp_senetence_index > current_senetence_index)
                {   
                    current_senetence_index = temp_senetence_index;
                    textRange.ApplyPropertyValue(TextElement.BackgroundProperty, null);
                    if (temp_senetence_index < index_sentence.Length-1)
                    {
                        TextRange tr1 = new TextRange(textRange.Start.GetPositionAtOffset(index_sentence[current_senetence_index]), textRange.Start.GetPositionAtOffset(index_sentence[current_senetence_index + 1] - 1));
                        tr1.ApplyPropertyValue(TextElement.BackgroundProperty, color);
                    }
                    else
                    {
                        TextRange tr1 = new TextRange(textRange.Start.GetPositionAtOffset(index_sentence[current_senetence_index]), textRange.End);
                        tr1.ApplyPropertyValue(TextElement.BackgroundProperty, color);
                    } 
                }
            }
        }

        private TextRange FindSentenceFromPosition(TextPointer position, string word)
        {
            while (position != null)
            {
                if (position.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {

                    string textRun = position.GetTextInRun(LogicalDirection.Forward);
                    // Find the starting index of any substring that matches "word".
                    int indexInRun = textRun.IndexOf(word);
                    if (indexInRun >= 0)
                    {
                        //Console.WriteLine(indexInRun);
                        TextPointer start = position.GetPositionAtOffset(indexInRun);
                        TextPointer end = start.GetPositionAtOffset(word.Length);
                        return new TextRange(start, end);
                    }
                }
                position = position.GetNextContextPosition(LogicalDirection.Forward);
            }
            // position will be null if "word" is not found.
            return null;
        }

        private List<int> HighlightSentencePre(TextRange textRange)
        {
            Regex rx = new Regex(@"(\S.+?[.!?])(?=\s+|$)");
            MatchCollection mc = rx.Matches(textRange.Text);
            List<int> sentence_index = new List<int>();
            List<int> sentence_length = new List<int>();
         
            foreach (Match match in mc)
            {
                sentence_index.Add(match.Index);
                sentence_length.Add(match.Length);
            }

            //int[] _temp = sentence_index.ToArray();

            if (sentence_index.Count == 0)
            {
                sentence_index.Add(textRange.Text.Length);
            }
            else
            {
                if (sentence_index.Last() + sentence_length.Last() < textRange.Text.Length)
                {
                    sentence_index.Add(sentence_index.Last() + sentence_length.Last() + 1);
                    //sentence_length.Add(textRange.Text.Length - sentence_index.Last() + sentence_length.Last());
                }
            }
            return sentence_index;
        }
    }
}
